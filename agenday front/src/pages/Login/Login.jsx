import React, { useState } from 'react';
import './Login.css';
import UserService from '../../services/UserService';

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const handleLogin = async (e) => {
    e.preventDefault();

    const userService = new UserService();

    try {
      const loginSuccess = await userService.login({ email, password });

      if (loginSuccess) {
        // Redirecionar para outra página após o login bem-sucedido
        window.location.href = '/home-instituicao';
      } else {
        setErrorMessage('E-mail ou senha incorretos.');
      }
    } catch (error) {
      console.error('Error:', error);
      setErrorMessage('Ocorreu um erro. Por favor, tente novamente mais tarde.');
    }
  };

  return (
    <div className="login-page">
      <div className="login-container">
        <div className="login">
          <h2>Login</h2>
          <div className="form-container">
            <div className="form-section">
              {errorMessage && <div className="error-message">{errorMessage}</div>}
              <div className="input-group">
                <label>E-mail: <span className="required">*</span></label>
                <input type="email" name="email" required onChange={(e) => setEmail(e.target.value)} />
              </div>
              <div className="input-group">
                <label>Senha: <span className="required">*</span></label>
                <input type="password" name="password" required onChange={(e) => setPassword(e.target.value)} />
                <a href="/recuperar-senha" className="form-link">Esqueceu sua senha?</a>
              </div>
            </div>
          </div>
          <div className="form-buttons">
            <button type="submit" className="submit-button" onClick={(e) => handleLogin(e)}>Entrar</button>
          </div>
          <div>
            <a href="/cadastro-usuario" className="form-link" style={{ textAlign: 'center' }}>Ainda não tem cadastro? Inscreva-se agora!</a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
