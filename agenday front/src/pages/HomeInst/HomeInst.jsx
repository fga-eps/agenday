import React from 'react';
import { Link } from 'react-router-dom';
import styles from './HomeInst.module.css'; // Importação correta do CSS
import homeImage from '../../imagens/Home-Image.png';
import '../../global.css';

const HomeInst = () => {
  return (
    <div className={styles.home}>
      <div className={styles.breadcrumb}>
        <Link to="/">Home</Link> / <span>Instituição</span>
      </div>
      <div className={styles.mainContent}>
        <div className={styles.buttons}>
          <header className={styles.header}>
            <h1>Selecione a operação desejada:</h1>
          </header>
          <Link to="/gerenciamento-inst" className={styles.btn}>Gerenciar Instituição</Link>
          <Link to="/cadastro-profissional" className={styles.btn}>Gerenciar Funcionários</Link>
          <Link to="/visualizar-agendamento" className={styles.btn}>Visualizar agenda</Link>
          <Link to="/gerenciamento-especialidades" className={styles.btn}>Gerenciar Especialidades/Serviços</Link>
        </div>
        <div className={styles.imageContainer}>
          <img src={homeImage} alt="Home" />
        </div>
      </div>
      <nav className={styles.navbar}>
        <Link to="/" className={styles.btnBack}>Voltar</Link>
      </nav>
    </div>
  );
};

export default HomeInst;
