import React, { useState, useEffect } from 'react';
import './GerenciamentoEspecialidades.css';
import axios from 'axios';
import { Link } from 'react-router-dom'; // Importar Link

const handleCancel = () => {
  window.location.href = '/';
};
const path = import.meta.env.VITE_API_URL;


const EspecialidadeDropdown = ({ especialidade, descricao, onDelete }) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  return (
    <div className="especialidade">
      <div className="especialidade-header" onClick={toggleDropdown}>
        <span>{especialidade.id}</span>
        <input type="text" value={especialidade.nome} readOnly />
        <button className="delete-button" onClick={(e) => { e.stopPropagation(); onDelete(especialidade.id); }}>Excluir</button>
      </div>
      {isOpen && (
        <div className="especialidade-content">
          <div className="descricao-container">
            <label>Descrição:</label>
            <textarea value={descricao} readOnly />
          </div>
        </div>
      )}
    </div>
  );
};

const GerenciamentoEspecialidades = () => {
  const [especialidades, setEspecialidades] = useState([]);
  const [opcoesEspecialidades, setOpcoesEspecialidades] = useState([]);
  const [showCadastro, setShowCadastro] = useState(false);
  const [novaEspecialidade, setNovaEspecialidade] = useState({ id: '', descricao: '' });
  const [editingEspecialidade, setEditingEspecialidade] = useState(null);

  useEffect(() => {
    const institutionId = localStorage.getItem('instituitionId');
    const token = localStorage.getItem('Token');
    
    if (institutionId) {
      axios.get(`${path}/registry/specialties/institutions/${institutionId}`, {
        headers: {
          'Authorization': `Bearer ${token}`,
          'Accept': 'application/json'
        }
      })
        .then(response => {
          const specialties = response.data.map(specialty => ({
            id: specialty.id,
            nome: specialty.name,
            descricao: specialty.description,
          }));
          setEspecialidades(specialties);
        })
        .catch(error => {
          console.error("Houve um erro ao buscar as especialidades:", error);
        });
    } else {
      console.error("institutionId não encontrado no localStorage");
    }

    axios.get(`${path}/registry/specialties`, {
      headers: {
        'Authorization': `Bearer ${token}`,
        'Accept': 'application/json'
      }
    })
      .then(response => {
        setOpcoesEspecialidades(response.data);
      })
      .catch(error => {
        console.error("Houve um erro ao buscar as opções de especialidades:", error);
      });
  }, []);

  const handleShowCadastro = () => {
    setShowCadastro(!showCadastro);
    setEditingEspecialidade(null);
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setNovaEspecialidade({ ...novaEspecialidade, [name]: value });

    if (name === 'id') {
      const especialidadeSelecionada = opcoesEspecialidades.find(opcao => opcao.id === parseInt(value));
      if (especialidadeSelecionada) {
        setNovaEspecialidade(prevState => ({
          ...prevState,
          descricao: especialidadeSelecionada.description
        }));
      }
    }
  };

  const handleCadastrarEspecialidade = () => {
    const institutionId = localStorage.getItem('instituitionId');
    const token = localStorage.getItem('Token');
    
    if (!institutionId) {
      console.error("institutionId não encontrado no localStorage");
      return;
    }

    const especialidadeJaCadastrada = especialidades.some(especialidade => especialidade.id === parseInt(novaEspecialidade.id));
    if (especialidadeJaCadastrada) {
      console.error("Especialidade já cadastrada");
      alert("Especialidade já cadastrada");
      return;
    }

    axios.post(`${path}/registry/specialties/institutions/${institutionId}/add/${novaEspecialidade.id}`, {}, {
      headers: {
        'Authorization': `Bearer ${token}`,
        'Accept': 'application/json'
      }
    })
      .then(response => {
        if (response.status === 200) {
          const especialidadeSelecionada = opcoesEspecialidades.find(especialidade => especialidade.id === parseInt(novaEspecialidade.id));
          const nova = { id: especialidadeSelecionada.id, nome: especialidadeSelecionada.name, descricao: especialidadeSelecionada.description };
          setEspecialidades([...especialidades, nova]);
          setShowCadastro(false);
          setNovaEspecialidade({ id: '', descricao: '' });
        } else {
          console.error("Erro ao cadastrar a especialidade:", response.data);
        }
      })
      .catch(error => {
        console.error("Houve um erro ao cadastrar a especialidade:", error);
      });
  };

  const handleDelete = (id) => {
    const institutionId = localStorage.getItem('instituitionId');
    const token = localStorage.getItem('Token');
    
    if (institutionId) {
      axios.post(`${path}/registry/specialties/institutions/${institutionId}/remove/${id}`, {}, {
        headers: {
          'Authorization': `Bearer ${token}`,
          'Accept': 'application/json'
        }
      })
        .then(response => {
          if (response.status === 200) {
            const updatedEspecialidades = especialidades.filter((esp) => esp.id !== id);
            setEspecialidades(updatedEspecialidades);
          } else {
            console.error("Erro ao excluir a especialidade:", response.data);
          }
        })
        .catch(error => {
          console.error("Houve um erro ao excluir a especialidade:", error);
        });
    } else {
      console.error("institutionId não encontrado no localStorage");
    }
  };

  return (
    <div>
      <div className="breadcrumb">
        <Link to="/">Home</Link> / <Link to="/home-instituicao">Menu Instituição</Link> / <span>Especialidades</span>
      </div>
      <div className="gerenciamento-especialidades">
        <h2>Gerenciamento de Especialidades</h2>
        <div className="especialidade-container">
          {especialidades.map((especialidade) => (
            <EspecialidadeDropdown
              key={especialidade.id}
              especialidade={especialidade}
              descricao={especialidade.descricao}
              onDelete={handleDelete}
            />
          ))}
        </div>
        {showCadastro && (
          <div className="cadastro-especialidade">
            <h3>{editingEspecialidade !== null ? 'Editar Especialidade' : 'Cadastrar Nova Especialidade'}</h3>
            <div>
              <label>Nome:</label>
              <select
                name="id"
                value={novaEspecialidade.id}
                onChange={handleChange}
              >
                <option value="">Selecione uma especialidade</option>
                {opcoesEspecialidades.map(opcao => (
                  <option key={opcao.id} value={opcao.id}>
                    {opcao.name}
                  </option>
                ))}
              </select>
            </div>
            <div>
              <label>Descrição:</label>
              <textarea
                name="descricao"
                value={novaEspecialidade.descricao}
                readOnly
              />
            </div>
            <div className="buttons-container">
              <button className="cancel-button" onClick={handleCancel}>Cancelar</button>
              <button className="submit-button" onClick={handleCadastrarEspecialidade}>
                {editingEspecialidade !== null ? 'Salvar' : 'Cadastrar'}
              </button>
            </div>
          </div>
        )}
        {!showCadastro && (
          <div className="buttons-container">
            <button className="submit-button" onClick={handleShowCadastro}>Cadastrar especialidade</button>
          </div>
        )}
      </div>
    </div>
  );
};

export default GerenciamentoEspecialidades;
