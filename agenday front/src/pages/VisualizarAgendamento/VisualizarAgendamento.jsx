import React, { useState, useEffect } from 'react';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import ListaProfissionais from '../../components/ListaProfissionais/ListaProfissionais';
import TabelaAgendamentos from '../../components/TabelaAgendamentos/TabelaAgendamentos';
import styles from './VisualizarAgendamento.module.css';

const path = import.meta.env.VITE_API_URL;
console.log('API URL:', path); 

const VisualizarAgendamento = () => {
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [selectedProfessional, setSelectedProfessional] = useState(null);
  const [appointments, setAppointments] = useState([]);
  const [professionals, setProfessionals] = useState([]);
  const institutionId = localStorage.getItem('instituitionId');

  useEffect(() => {
    const fetchAppointments = async () => {
      try {
        const token = localStorage.getItem('Token');
        const response = await axios.get(`${path}/registry/appointments/institution/${institutionId}`, {
          headers: {
            'Authorization': `Bearer ${token}`,
            'Accept': 'application/json'
          }
        });
        const appointmentsData = response.data;
        console.log("Fetched appointments:", appointmentsData);
        setAppointments(appointmentsData);

        // Extract unique professionals
        const uniqueProfessionalsMap = new Map();
        appointmentsData.forEach(appointment => {
          if (!uniqueProfessionalsMap.has(appointment.employeeName)) {
            uniqueProfessionalsMap.set(appointment.employeeName, appointment.specialty);
          }
        });

        const uniqueProfessionals = Array.from(uniqueProfessionalsMap).map(([name, specialty]) => ({
          name,
          specialty
        }));

        setProfessionals(uniqueProfessionals);
      } catch (error) {
        console.error('Error fetching appointments', error);
      }
    };

    fetchAppointments();
  }, [institutionId]);

  const handleDateChange = (date) => {
    setSelectedDate(date);
  };

  const handleSelectProfessional = (professionalName) => {
    setSelectedProfessional(professionalName);
  };

  const formatDateString = (date) => {
    const d = new Date(date);
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();
    const year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  };

  const formattedSelectedDate = formatDateString(selectedDate);

  const filteredAppointments = appointments.filter(a => {
    const appointmentDate = a.date;
    console.log("Comparing dates:", appointmentDate, formattedSelectedDate);
    return (
      (!selectedProfessional || a.employeeName === selectedProfessional) &&
      appointmentDate === formattedSelectedDate
    );
  });

  console.log("Filtered appointments:", filteredAppointments);

  return (
    <div className={styles.visualizarAgendamento}>
      <div className={styles.conteudo}>
        <div className={styles.datePickerContainer}>
          <label className={styles.dateLabel}>Selecione uma Data de Busca:</label>
          <DatePicker selected={selectedDate} onChange={handleDateChange} dateFormat="yyyy-MM-dd" className={styles.datePicker} />
        </div>
        <ListaProfissionais
          professionals={professionals}
          selectedProfessional={selectedProfessional}
          onSelectProfessional={handleSelectProfessional}
        />
        <TabelaAgendamentos appointments={filteredAppointments} />
      </div>
    </div>
  );
};

export default VisualizarAgendamento;
