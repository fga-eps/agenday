import React, { useEffect, useState } from 'react';
import { useNavigate, Link } from 'react-router-dom';
import styles from './HomeUsr.module.css';
import homeImage from '../../imagens/Home-Image.png';
import { Button, TIPO_BUTTON } from '../../components';
import '../../global.css';

const path = import.meta.env.VITE_API_URL;

const HomeUsr = () => {
  const [instituicoes, setInstituicoes] = useState([]);
  const [especialidades, setEspecialidades] = useState([]);
  const [profissionais, setProfissionais] = useState([]);
  const [selectedInstituicao, setSelectedInstituicao] = useState('');
  const [selectedEspecialidade, setSelectedEspecialidade] = useState('');
  const [selectedProfissional, setSelectedProfissional] = useState('');
  const [selectedGeolocation, setSelectedGeolocation] = useState({ lat: '', lng: '' });
  const navigate = useNavigate();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const token = localStorage.getItem('Token'); // Obtém o token do localStorage
        const response = await fetch(`${path}/registry/services/provided/get/all`, {
          headers: {
            'Authorization': `Bearer ${token}`,
            'Accept': 'application/json'
          }
        });
        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }
        const data = await response.json();
        const instituicoesData = data.servicesType.map(service => service.institutions).flat();
        setInstituicoes(instituicoesData);
      } catch (error) {
        console.error('Erro ao buscar dados:', error);
      }
    };

    fetchData();
  }, []);

  useEffect(() => {
    if (selectedInstituicao) {
      const instituicao = instituicoes.find(inst => inst.name === selectedInstituicao);
      if (instituicao) {
        setEspecialidades(instituicao.speciaties || []);
        setSelectedGeolocation({
          lat: instituicao.geolocationDTO.latitude,
          lng: instituicao.geolocationDTO.longitude
        });
        setProfissionais([]); // Limpa os profissionais quando uma nova instituição é selecionada
      }
    }
  }, [selectedInstituicao, instituicoes]);

  useEffect(() => {
    if (selectedEspecialidade) {
      const instituicao = instituicoes.find(inst => inst.name === selectedInstituicao);
      if (instituicao) {
        const especialidade = instituicao.speciaties.find(spec => spec.name === selectedEspecialidade);
        if (especialidade) {
          setProfissionais(especialidade.employes || []);
        }
      }
    }
  }, [selectedEspecialidade, instituicoes]);

  const handleSearch = () => {
    if (selectedProfissional) {
      navigate(`/agendamento?profissionalId=${selectedProfissional}&lat=${selectedGeolocation.lat}&lng=${selectedGeolocation.lng}`);
    } else {
      alert('Por favor, selecione um profissional.');
    }
  };

  return (
    <div className={styles.home}>
      <div className={styles.breadcrumb}>
        <Link to="/">Home</Link> / <span>Agendamento</span>
      </div>
      <div className={styles.mainContent}>
        <div className={styles.filtros}>
          <header className={styles.header}>
            <h1>Agende sua consulta conosco!</h1>
            <p>Selecione a instituição, especialidade e o profissional, escolha o horário e agende, pronto!</p>
          </header>
          <div className={styles.content}>
            <div className={styles.inputGroup}>
              <select name="instituicao" required onChange={(e) => setSelectedInstituicao(e.target.value)}>
                <option value="">Selecione a instituição desejada:</option>
                {instituicoes.map((instituicao) => (
                  <option key={instituicao.name} value={instituicao.name}>
                    {instituicao.name}
                  </option>
                ))}
              </select>
            </div>
            <div className={styles.inputGroup}>
              <select name="especialidade" required onChange={(e) => setSelectedEspecialidade(e.target.value)}>
                <option value="">Selecione a especialidade desejada:</option>
                {especialidades.map((especialidade) => (
                  <option key={especialidade.name} value={especialidade.name}>
                    {especialidade.name}
                  </option>
                ))}
              </select>
            </div>
            <div className={styles.inputGroup}>
              <select name="profissional" required onChange={(e) => setSelectedProfissional(e.target.value)}>
                <option value="">Selecione o profissional desejado:</option>
                {profissionais.map((profissional) => (
                  <option key={profissional.id} value={profissional.id}>
                    {profissional.name}
                  </option>
                ))}
              </select>
            </div>
            <Button type={TIPO_BUTTON.PRIMARY} className={styles.btn} onClick={handleSearch}>Pesquisar</Button>
          </div>
        </div>
        <div className={styles.imageContainer}>
          <img src={homeImage} alt="Home" />
        </div>
      </div>
      <nav className={styles.navbar}>
        <Link to="/" className={styles.btnBack}>Voltar</Link>
      </nav>
    </div>
  );
};

export default HomeUsr;
