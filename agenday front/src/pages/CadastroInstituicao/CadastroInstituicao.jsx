import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import styles from './CadastroInstituicao.module.css';
import UserService from '../../services/UserService';

const path = import.meta.env.VITE_API_URL;


const CadastroInstituicao = () => {
  const [name, setName] = useState('');
  const [telefone, setTelefone] = useState('');
  const [endereco, setEndereco] = useState('');
  const [cep, setCep] = useState('');
  const [bairro, setBairro] = useState('');
  const [document, setDocument] = useState('');
  const [numero, setNumero] = useState('');
  const [complemento, setComplemento] = useState('');
  const [errors, setErrors] = useState({});
  const [cidades, setCidades] = useState([]);


  useEffect(() => {
    fetchCidades();
  }, []);

  const fetchCidades = async () => {
    const token = localStorage.getItem('Token');

    try {
      const response = await axios.get(`${path}/registry/states/get/all`, {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      });
      const cities = response.data.flatMap(state => state.cities);
      setCidades(cities);
    } catch (error) {
      console.error('Erro ao buscar cidades:', error);
    }
  };

  const validateCNPJ = (cnpj) => {
    cnpj = cnpj.replace(/[^\d]+/g, '');
    if (cnpj.length !== 14) return false;
    // Verificação do CNPJ
    let tamanho = cnpj.length - 2;
    let numeros = cnpj.substring(0, tamanho);
    const digitos = cnpj.substring(tamanho);
    let soma = 0;
    let pos = tamanho - 7;
    for (let i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2) pos = 9;
    }
    let resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado !== parseInt(digitos.charAt(0))) return false;

    tamanho += 1;
    numeros = cnpj.substring(0, tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (let i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2) pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado !== parseInt(digitos.charAt(1))) return false;

    return true;
  };

  const validateTelefone = (telefone) => {
    const regex = /^\(\d{2}\)\s\d{4,5}-\d{4}$/;
    return regex.test(telefone);
  };

  const validateCEP = (cep) => {
    const regex = /^\d{5}-\d{3}$/;
    return regex.test(cep);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    setErrors({});

    // Validações
    const newErrors = {};
    if (!validateCNPJ(document)) {
      newErrors.document = 'CNPJ inválido';
    }
    if (!validateTelefone(telefone)) {
      newErrors.telefone = 'Telefone inválido';
    }
    if (!validateCEP(cep)) {
      newErrors.cep = 'CEP inválido';
    }

    if (Object.keys(newErrors).length > 0) {
      setErrors(newErrors);
      return;
    }

    const userService = new UserService();
    const userId = localStorage.getItem('id');

    if (!userId) {
      setErrors({ general: 'ID do usuário não encontrado. Por favor, faça login novamente.' });
      return;
    }

    const institutionData = {
      id: 0,
      name: name.toUpperCase(),
      type: 'CNPJ',
      document: document.toUpperCase(),
      serviceTypeId: 1,
      addressDTO: {
        street: endereco.toUpperCase(),
        city: parseInt(bairro),
        zipCode: cep.toUpperCase(),
        complement: complemento.toUpperCase(),
        number: numero.toUpperCase()
      },
      contactDTO: [
        {
          type: 'telefone',
          value: telefone.toUpperCase()
        }
      ]
    };

    try {
      const success = await userService.cadastrarInstituicao(institutionData);

      if (success) {
        window.location.href = '/login';
      } else {
        setErrors({ general: 'Erro ao cadastrar a instituição. Por favor, tente novamente.' });
      }
    } catch (error) {
      console.error('Erro ao cadastrar a instituição:', error);
      if (error.response && error.response.data && error.response.data.errors) {
        setErrors(error.response.data.errors);
      } else {
        setErrors({ general: 'Erro ao cadastrar a instituição. Por favor, tente novamente.' });
      }
    }
  };

  const handleCancel = () => {
    window.location.href = '/';
  };

  return (
    <div>
      <div className={styles.breadcrumb}>
        <Link to="/">Home</Link> / <Link to="/home-instituicao">Menu Instituição</Link> / <span>Cadastro Instituição</span>
      </div>
      <div className={styles.cadastroInstituicao}>
        <h2>Cadastro de Instituição</h2>
        {errors.general && <div className={styles.errorMessage}>{errors.general}</div>}
        <form onSubmit={handleSubmit} className={styles.formContainer}>
          <div className={styles.formSection}>
            <h3>Informações da Instituição</h3>
            <div className={styles.inputGroup}>
              <label>Nome da Instituição: <span className={styles.required}>*</span></label>
              <input type="text" name="name" value={name} onChange={(e) => setName(e.target.value)} required />
              {errors.name && <div className={styles.errorMessage}>{errors.name}</div>}
            </div>
            <div className={styles.inputGroup}>
              <label>CNPJ: <span className={styles.required}>*</span></label>
              <input type="text" name="document" value={document} onChange={(e) => setDocument(e.target.value)} required />
              {errors.document && <div className={styles.errorMessage}>{errors.document}</div>}
            </div>
            <div className={styles.inputGroup}>
              <label>Telefone: <span className={styles.required}>*</span></label>
              <input type="tel" name="telefone" value={telefone} onChange={(e) => setTelefone(e.target.value)} required />
              {errors.telefone && <div className={styles.errorMessage}>{errors.telefone}</div>}
            </div>
            <div className={styles.inputGroup}>
              <label>Endereço: <span className={styles.required}>*</span></label>
              <input type="text" name="endereco" value={endereco} onChange={(e) => setEndereco(e.target.value)} required />
              {errors.endereco && <div className={styles.errorMessage}>{errors.endereco}</div>}
            </div>
            <div className={styles.inputGroup}>
              <label>Bairro: <span className={styles.required}>*</span></label>
              <select name="bairro" value={bairro} onChange={(e) => setBairro(e.target.value)} required>
                <option value="">Selecione</option>
                {cidades.map(cidade => (
                  <option key={cidade.id} value={cidade.id}>
                    {cidade.name}
                  </option>
                ))}
              </select>
              {errors.bairro && <div className={styles.errorMessage}>{errors.bairro}</div>}
            </div>
            <div className={styles.inputGroup}>
              <label>CEP: <span className={styles.required}>*</span></label>
              <input type="text" name="cep" value={cep} onChange={(e) => setCep(e.target.value)} required />
              {errors.cep && <div className={styles.errorMessage}>{errors.cep}</div>}
            </div>
            <div className={styles.inputGroup}>
              <label>Número: <span className={styles.required}>*</span></label>
              <input type="text" name="numero" value={numero} onChange={(e) => setNumero(e.target.value)} required />
              {errors.numero && <div className={styles.errorMessage}>{errors.numero}</div>}
            </div>
            <div className={styles.inputGroup}>
              <label>Complemento:</label>
              <input type="text" name="complemento" value={complemento} onChange={(e) => setComplemento(e.target.value)} />
              {errors.complemento && <div className={styles.errorMessage}>{errors.complemento}</div>}
            </div>
          </div>
          <div className={styles.formButtons}>
            <button type="button" className={styles.cancelButton} onClick={handleCancel}>Cancelar</button>
            <button type="submit" className={styles.submitButton}>Cadastrar</button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default CadastroInstituicao;
