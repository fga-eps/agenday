import React from 'react';
import { Link } from 'react-router-dom';
import './HomeSeparacao.css';
import homeImage from '../../imagens/Home-Image.png';
import '../../global.css';

const HomeSeparacao = () => {
  const handleCompanyClick = (e) => {
    const userId = localStorage.getItem('id');
    if (!userId) {
      e.preventDefault(); // Evita que o link seja seguido
      window.location.href = '/login'; // Redireciona para login
    }
  };

  return (
    <div className="home">
      <div className="buttons">
        <header className="header">
          <h1>Simplificando o seu atendimento, maximizando a sua saúde.</h1>
          <p>Conheça nossas soluções de agendamento.</p>
        </header>
        <div className="content">
          <div className="options">
            <div className="option">
              <p>Encontre e agende serviços de saúde de forma simples e eficiente.</p>
              <Link to="/home-usuario" className="btn btn-user">Sou Usuário</Link>
            </div>
            <div className="option">
              <p>Gerencie agendas de profissionais de saúde de maneira eficiente.</p>
              <Link to="/home-instituicao" className="btn btn-company" onClick={handleCompanyClick}>
                Sou Empresa
              </Link>
            </div>
          </div>
        </div>
      </div>
      <div className="image-container">
        <img src={homeImage} alt="HomeUsr" />
      </div>
    </div>
  );
};

export default HomeSeparacao;
