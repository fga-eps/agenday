import React, { useState } from 'react';
import DisponibilidadeHorarios from '../../components/DisponibilidadeHorarios/DisponibilidadeHorarios';
import styles from './Agendamento.module.css';
import axios from 'axios';
import Swal from 'sweetalert2';
import Localizacao from '../../components/Localizacao/Localizacao';

const path = import.meta.env.VITE_API_URL;


const Agendamento = () => {
  const urlParams = new URLSearchParams(window.location.search);
  const profissionalId = urlParams.get('profissionalId');
  const lat = parseFloat(urlParams.get('lat'));
  const lng = parseFloat(urlParams.get('lng'));

  const [formData, setFormData] = useState({
    name: '',
    email: '',
    cpf: '',
    phone: '',
    dateBirth: '',
  });

  const [selectedSlot, setSelectedSlot] = useState(null);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleCancel = () => {
    window.location.href = '/';
  };


  const handleSubmit = () => {
    if (selectedSlot) {
      const payload = {
        employeeId: profissionalId,
        date: selectedSlot.date,
        time: selectedSlot.startTime,
        client: formData,
      };

      axios.post(`${path}/registry/appointments`, payload)
        .then(response => {
          Swal.fire({
            title: 'Agendado com sucesso!',
            text: 'Seu agendamento foi realizado com sucesso.',
            icon: 'success',
            confirmButtonText: 'OK'
          });
        })
        .catch(error => {
          console.error('Error creating appointment', error);
          Swal.fire({
            title: 'Erro',
            text: 'Ocorreu um erro ao tentar agendar. Por favor, tente novamente.',
            icon: 'error',
            confirmButtonText: 'OK'
          });
        });
    } else {
      Swal.fire({
        title: 'Erro',
        text: 'Por favor, selecione um horário.',
        icon: 'error',
        confirmButtonText: 'OK'
      });
    }
  };

  return (
    <div className={styles.container}>
      <div className={styles.formContainer}>
        <h2>Informações do Paciente</h2>
        <div className={styles.inputGroup}>
          <label>Nome: <span className={styles.required}>*</span></label>
          <input type="text" name="name" value={formData.name} onChange={handleInputChange} required />
        </div>
        <div className={styles.inputGroup}>
          <label>E-mail: <span className={styles.required}>*</span></label>
          <input type="email" name="email" value={formData.email} onChange={handleInputChange} required />
        </div>
        <div className={styles.inputGroup}>
          <label>CPF: <span className={styles.required}>*</span></label>
          <input type="text" name="cpf" value={formData.cpf} onChange={handleInputChange} required />
        </div>
        <div className={styles.inputGroup}>
          <label>Telefone: <span className={styles.required}>*</span></label>
          <input type="text" name="phone" value={formData.phone} onChange={handleInputChange} required />
        </div>
        <div className={styles.inputGroup}>
          <label>Data de Nascimento: <span className={styles.required}>*</span></label>
          <input type="date" name="dateBirth" value={formData.dateBirth} onChange={handleInputChange} required />
        </div>
        <div className={styles.buttons}>
          <button className={styles.submitButton} onClick={handleSubmit}>Agendar</button>
          <button className={styles.cancelButton} onClick={handleCancel}>Cancel</button>
        </div>
      </div>
      <div className={styles.rightContainer}>
        <div className={styles.horariosContainer}>
          <h2>Escolha o horário de atendimento</h2>
          <DisponibilidadeHorarios profissionalId={profissionalId} onTimeSlotSelect={setSelectedSlot} />
        </div>
        <div className={styles.mapContainer}>
          <Localizacao lat={lat} lng={lng} />
        </div>
      </div>
    </div>
  );
};

export default Agendamento;
