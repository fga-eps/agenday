import React, { useState } from 'react';
import './DadosAgendamento.css';
import doctorImage from '../../imagens/doctor.png';
import Swal from 'sweetalert2';
import axios from 'axios';

const DadosAgendamento = () => {
  const [formData, setFormData] = useState({
    nome: '',
    email: '',
    cpf: '',
    telefone: ''
  });
  const [errorMessage, setErrorMessage] = useState('');

  const validateEmail = (email) => {
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return regex.test(email);
  };

  const validateCPF = (cpf) => {
    cpf = cpf.replace(/[^\d]+/g, '');
    if (cpf.length !== 11) return false;

    let soma = 0;
    let resto;
    if (cpf === "00000000000") return false;

    for (let i = 1; i <= 9; i++) soma = soma + parseInt(cpf.substring(i - 1, i)) * (11 - i);
    resto = (soma * 10) % 11;

    if ((resto === 10) || (resto === 11)) resto = 0;
    if (resto !== parseInt(cpf.substring(9, 10))) return false;

    soma = 0;
    for (let i = 1; i <= 10; i++) soma = soma + parseInt(cpf.substring(i - 1, i)) * (12 - i);
    resto = (soma * 10) % 11;

    if ((resto === 10) || (resto === 11)) resto = 0;
    if (resto !== parseInt(cpf.substring(10, 11))) return false;
    return true;
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value
    });
  };

  const handleAgendarClick = async () => {
    const { nome, email, cpf, telefone } = formData;

    if (!validateEmail(email)) {
      setErrorMessage('E-mail inválido');
      return;
    }

    if (!validateCPF(cpf)) {
      setErrorMessage('CPF inválido');
      return;
    }

    setErrorMessage('');

    const dataToSend = {
      nome: nome.toUpperCase(),
      email: email.toUpperCase(),
      cpf: cpf.toUpperCase(),
      telefone: telefone.toUpperCase()
    };

    try {
      const response = await axios.post('URL_DO_SEU_BACKEND', dataToSend);
      if (response.status === 200 || response.status === 201) {
        Swal.fire({
          title: 'Agendado com sucesso!',
          text: 'Seu agendamento foi realizado com sucesso.',
          icon: 'success',
          confirmButtonText: 'OK'
        });
      } else {
        setErrorMessage('Falha ao agendar. Por favor, tente novamente.');
      }
    } catch (error) {
      console.error('Error:', error);
      setErrorMessage('Erro ao agendar. Por favor, tente novamente.');
    }
  };

  return (
    <div className="login-container">
      <img src={doctorImage} alt="Doctor" className="doctor-image" />
      <div className="dados-agendamento">
        <h2>Informações do Paciente</h2>
        {errorMessage && <div className="error-message">{errorMessage}</div>}
        <div className="form-container">
          <div className="form-section">
            <div className="input-group">
              <label>Nome: <span className="required">*</span></label>
              <input type="text" name="nome" value={formData.nome} onChange={handleInputChange} required />
            </div>
            <div className="input-group">
              <label>E-mail: <span className="required">*</span></label>
              <input type="text" name="email" value={formData.email} onChange={handleInputChange} required />
            </div>
            <div className="input-group">
              <label>CPF: </label>
              <input type="text" name="cpf" value={formData.cpf} onChange={handleInputChange} required />
            </div>
            <div className="input-group">
              <label>Telefone: <span className="required">*</span></label>
              <input type="text" name="telefone" value={formData.telefone} onChange={handleInputChange} required />
            </div>
          </div>
        </div>
        <div className="form-buttons">
          <button type="submit" className="submit-button" onClick={handleAgendarClick}>Agendar</button>
        </div>
      </div>
    </div>
  );
};

export default DadosAgendamento;
