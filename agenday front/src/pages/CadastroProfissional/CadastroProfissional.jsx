import React, { useEffect, useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import styles from './CadastroProfissional.module.css'; 
import Horarios from '../../components/Horarios/Horarios';
import { Link } from 'react-router-dom';
import axios from 'axios';

const path = import.meta.env.VITE_API_URL;



const CadastroProfissional = () => {
    const [specialties, setSpecialties] = useState([]);
    const [selectedSpecialties, setSelectedSpecialties] = useState([]);
    const [selectedTimeSlots, setSelectedTimeSlots] = useState([]);
    const [employeeId, setEmployeeId] = useState(null);
    const [message, setMessage] = useState('');
    const [isTimeSlotsRegistered, setIsTimeSlotsRegistered] = useState(false);
    const institutionId = localStorage.getItem('instituitionId');
    const token = localStorage.getItem('Token');
    const navigate = useNavigate();
    const formRef = useRef(null);


    useEffect(() => {
        if (institutionId && token) {
            axios.get(`${path}/registry/specialties/institutions/${institutionId}`, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Accept': 'application/json'
                }
            })
            .then(response => {
                setSpecialties(response.data);
            })
            .catch(error => {
                console.error("There was an error fetching the specialties!", error);
            });
        }
    }, [institutionId, token]);

    const handleTimeSlotChange = (timeSlots) => {
        setSelectedTimeSlots(timeSlots);
    };

    const handleRegisterEmployee = async (event) => {
        event.preventDefault();

        const employeeData = {
            name: event.target['nome-profissional'].value.toUpperCase(),
            cpf: event.target['cpf-profissional'].value.toUpperCase(),
            email: event.target['email-profissional'].value.toUpperCase(),
            institutionId: institutionId,
            specialtyIds: selectedSpecialties,
            registro: event.target['registro'].value.toUpperCase(),
            estado: event.target['estado'].value.toUpperCase()
        };

        try {
            const response = await axios.post(`${path}/registry/employees`, employeeData, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            });

            if (response.status === 200 || response.status === 201) {
                console.log("Employee registered successfully:", response.data);
                setEmployeeId(response.data.id);
                setMessage('');
            } else {
                console.error("Unexpected response status:", response.status);
            }
        } catch (error) {
            console.error("There was an error registering the employee!", error);
        }
    };

    const handleRegisterTimeSlots = async () => {
        if (selectedTimeSlots.length === 0) {
            console.error("No time slots selected.");
            return;
        }

        const timeSlotsData = {
            employeeId: employeeId,
            timeSlots: selectedTimeSlots.map(slot => ({
                employeeId: employeeId,
                data: slot.data.toUpperCase(),
                startTime: slot.startTime.toUpperCase(),
                endTime: slot.endTime.toUpperCase(),
                available: true
            }))
        };

        try {
            const timeSlotsResponse = await axios.post(`${path}/registry/timeslots`, timeSlotsData, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            });

            if (timeSlotsResponse.status === 200 || timeSlotsResponse.status === 201) {
                console.log("Time slots registered successfully:", timeSlotsResponse.data);
                setMessage('Horários cadastrados com sucesso!');
                setIsTimeSlotsRegistered(true);
            } else {
                console.error("Unexpected response status for time slots:", timeSlotsResponse.status);
            }
        } catch (error) {
            console.error("There was an error registering the time slots!", error);
        }
    };

    const handleSpecialtyChange = (event) => {
        const selectedOptions = Array.from(event.target.selectedOptions);
        const selectedIds = selectedOptions.map(option => parseInt(option.value));
        setSelectedSpecialties(selectedIds);
    };

    const handleLogout = () => {
        navigate('/home-instituicao');
    };

    const handleNewEmployee = () => {
        setEmployeeId(null);
        setSelectedSpecialties([]);
        setSelectedTimeSlots([]);
        setMessage('');
        setIsTimeSlotsRegistered(false);
        if (formRef.current) {
            formRef.current.reset();
        }
    };

    return (
        <div className={styles.cadastroProfissional}>
            <h2>Cadastro de Profissional</h2>
            <div className={styles.formContainer}>
                <div className={styles.formSection}>
                    <h3>Informações do Profissional</h3>
                    <form ref={formRef} onSubmit={handleRegisterEmployee}>
                        <div className={styles.inputGroup}>
                            <label>Nome: <span className="required">*</span></label>
                            <input type="text" name="nome-profissional" required />
                        </div>
                        <div className={styles.inputGroup}>
                            <label>CPF: <span className="required">*</span></label>
                            <input type="text" name="cpf-profissional" required />
                        </div>
                        <div className={styles.inputGroup}>
                            <label>Email: <span className="required">*</span></label>
                            <input type="text" name="email-profissional" required />
                        </div>
                        <div className={styles.inputGroup}>
                            <label>Especialidade: <span className="required">*</span></label>
                            <select multiple name="especialidade-profissional" onChange={handleSpecialtyChange} required>
                                {specialties.map(specialty => (
                                    <option key={specialty.id} value={specialty.id}>{specialty.name}</option>
                                ))}
                            </select>
                        </div>
                        <div className={styles.inputRow}>
                            <div className={styles.inputGroup}>
                                <label>Registro: <span className={styles.required}>*</span></label>
                                <input type="text" name="registro" required />
                            </div>
                            <div className={styles.inputGroup}>
                                <label>Estado: <span className={styles.required}>*</span></label>
                                <select name="estado" required>
                                    <option value="">Selecione</option>
                                    <option value="AC">Acre</option>
                                    <option value="AL">Alagoas</option>
                                    <option value="AP">Amapá</option>
                                    <option value="AM">Amazonas</option>
                                    <option value="BA">Bahia</option>
                                    <option value="CE">Ceará</option>
                                    <option value="DF">Distrito Federal</option>
                                    <option value="ES">Espírito Santo</option>
                                    <option value="GO">Goiás</option>
                                    <option value="MA">Maranhão</option>
                                    <option value="MT">Mato Grosso</option>
                                    <option value="MS">Mato Grosso do Sul</option>
                                    <option value="MG">Minas Gerais</option>
                                    <option value="PA">Pará</option>
                                    <option value="PB">Paraíba</option>
                                    <option value="PR">Paraná</option>
                                    <option value="PE">Pernambuco</option>
                                    <option value="PI">Piauí</option>
                                    <option value="RJ">Rio de Janeiro</option>
                                    <option value="RN">Rio Grande do Norte</option>
                                    <option value="RS">Rio Grande do Sul</option>
                                    <option value="RO">Rondônia</option>
                                    <option value="RR">Roraima</option>
                                    <option value="SC">Santa Catarina</option>
                                    <option value="SP">São Paulo</option>
                                    <option value="SE">Sergipe</option>
                                    <option value="TO">Tocantins</option>
                                </select>
                            </div>
                        </div>
                        <button type="submit" className={styles.submitButton}>Cadastrar Profissional</button>
                    </form>
                </div>
                {employeeId && (
                    <div className={styles.formSection}>
                        <Horarios comando='Escolha os horários de atendimento' textoBotao='Adicionar Horários' onTimeSlotChange={handleTimeSlotChange} />
                        <button onClick={handleRegisterTimeSlots} className={styles.submitButton}>Cadastrar Horários</button>
                    </div>
                )}
            </div>
            {message && <div className={styles.message}>{message}</div>}
            {employeeId && (
                <div className={styles.formButtons}>
                    <button onClick={handleLogout} className={styles.cancelButton} disabled={!isTimeSlotsRegistered}>Sair</button>
                    <button onClick={handleNewEmployee} className={styles.submitButton} disabled={!isTimeSlotsRegistered}>Cadastrar Novo Funcionário</button>
                </div>
            )}
        </div>
    );
};

export default CadastroProfissional;
