import React, { useState } from 'react';
import './CadastroUsuario.css';
import UserService from '../../services/UserService';

const CadastroUsuario = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const [name, setName] = useState('');

  const validateEmail = (email) => {
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return regex.test(email);
  };

  const handleSignin = async (e) => {
    e.preventDefault();

    if (password !== confirmPassword) {
      setErrorMessage('As senhas não coincidem');
      return;
    }

    if (!validateEmail(email)) {
      setErrorMessage('E-mail inválido');
      return;
    }

    const userService = new UserService();

    const role = "ROLE_CUSTOMER";

    try {
      const signupSuccess = await userService.cadastrarUsuario({
        email: email.toUpperCase(),
        password: password.toUpperCase(),
        name: name.toUpperCase(),
        role: role.toUpperCase()
      });

      if (signupSuccess) {
        // Redirecionar para outra página após o cadastro bem-sucedido
        window.location.href = '/cadastro-instituicao';
      } else {
        setErrorMessage('Falha no cadastro da instituição');
      }
    } catch (error) {
      console.error('Error:', error);
      setErrorMessage('Erro ao cadastrar. Por favor, tente novamente.');
    }
  };

  const handleCancel = () => {
    window.location.href = '/';
  };

  return (
    <div className="cadastro-usuario">
      <h2>Cadastro de Usuário</h2>
      {errorMessage && <div className="error-message">{errorMessage}</div>}
      <form onSubmit={handleSignin} className="form-container">
        <div className="form-section">
          <h3>Informações do Usuário</h3>
          <div className="input-group">
            <label>Nome: <span className="required">*</span></label>
            <input type="text" name="nome" required onChange={(e) => setName(e.target.value)}/>
          </div>
          <div className="input-group">
            <label>Email: <span className="required">*</span></label>
            <input type="email" name="email" required onChange={(e) => setEmail(e.target.value)} />
          </div>
          <div className="input-group">
            <label>Senha: <span className="required">*</span></label>
            <input type="password" name="senha" required onChange={(e) => setPassword(e.target.value)} />
          </div>
          <div className="input-group">
            <label>Confirme a Senha: <span className="required">*</span></label>
            <input type="password" name="confirmarSenha" required onChange={(e) => setConfirmPassword(e.target.value)} />
          </div>
        </div>
        <div className="form-buttons">
          <button type="button" className="cancel-button" onClick={handleCancel}>Cancelar</button>
          <button type="submit" className="submit-button">Cadastrar</button>
        </div>
      </form>
    </div>
  );
};

export default CadastroUsuario;
