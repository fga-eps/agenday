import React, { useEffect, useRef } from 'react';

const Localizacao = ({ lat, lng }) => {
  const mapRef = useRef(null);

  useEffect(() => {
    const loadGoogleMaps = () => {
      const script = document.createElement('script');
      script.src = `https://maps.googleapis.com/maps/api/js?key=AIzaSyC3GFOtNA8Mg1w7rNee7pbqy6-BBcTjTes`;
      script.async = true;
      script.defer = true;
      script.onload = () => initMap();
      document.head.appendChild(script);
    };

    const initMap = () => {
      if (mapRef.current && window.google) {
        const map = new window.google.maps.Map(mapRef.current, {
          center: { lat: parseFloat(lat), lng: parseFloat(lng) },
          zoom: 10,
        });

        new window.google.maps.Marker({
          position: { lat: parseFloat(lat), lng: parseFloat(lng) },
          map: map,
        });
      }
    };

    if (!window.google) {
      loadGoogleMaps();
    } else {
      initMap();
    }
  }, [lat, lng]);

  return <div ref={mapRef} style={{ width: '100%', height: '400px' }} />;
};

export default Localizacao;
