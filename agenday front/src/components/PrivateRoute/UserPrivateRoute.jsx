import React from 'react';
import { Route, Navigate } from 'react-router-dom';

const UserPrivateRoute = ({ element: Component, ...rest }) => {
  const isAuthenticated = !!localStorage.getItem('token');
  const userType = localStorage.getItem('userType');

  return (
    <Route
      {...rest}
      element={isAuthenticated && userType === 'user' ? <Component /> : <Navigate to="/login" />}
    />
  );
};

export default UserPrivateRoute;
