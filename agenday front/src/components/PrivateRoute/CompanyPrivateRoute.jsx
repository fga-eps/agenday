import React from 'react';
import { Route, Navigate } from 'react-router-dom';

const CompanyPrivateRoute = ({ element: Component, ...rest }) => {
  const isAuthenticated = !!localStorage.getItem('token');
  const userType = localStorage.getItem('userType');

  return (
    <Route
      {...rest}
      element={isAuthenticated && userType === 'company' ? <Component /> : <Navigate to="/login" />}
    />
  );
};

export default CompanyPrivateRoute;
