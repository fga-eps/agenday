import React, { useState } from 'react';
import styles from './NavegacaoDatas.module.css';

const initialDates = [
  { day: 'Seg', date: '06 Mai' },
  { day: 'Ter', date: '07 Mai' },
  { day: 'Qua', date: '08 Mai' },
  { day: 'Qui', date: '09 Mai' },
  { day: 'Sex', date: '10 Mai' },
  { day: 'Sab', date: '11 Mai' },
  { day: 'Dom', date: '12 Mai' },
];

const NavegacaoDatas = () => {
  const [dates, setDates] = useState(initialDates);
  const [selected, setSelected] = useState(null);

  const handlePrev = () => {
    const newDates = dates.map(date => {
      const newDate = new Date(new Date(date.date).setDate(new Date(date.date).getDate() - 7));
      return {
        day: date.day,
        date: newDate.toLocaleDateString('pt-BR', { day: '2-digit', month: 'short' }),
      };
    });
    setDates(newDates);
  };

  const handleNext = () => {
    const newDates = dates.map(date => {
      const newDate = new Date(new Date(date.date).setDate(new Date(date.date).getDate() + 7));
      return {
        day: date.day,
        date: newDate.toLocaleDateString('pt-BR', { day: '2-digit', month: 'short' }),
      };
    });
    setDates(newDates);
  };

  const handleSelect = (index) => {
    setSelected(index);
  };

  return (
    <div className={styles.navegacaoContainer}>
      <button className={styles.navButton} onClick={handlePrev}>{'<'}</button>
      <div className={styles.calendar}>
        {dates.map((date, index) => (
          <div
            key={index}
            className={`${styles.day} ${selected === index ? styles.selected : ''}`}
            onClick={() => handleSelect(index)}
          >
            <span className={styles.dayOfWeek}>{date.day}</span>
            <span className={styles.dayNumber}>{date.date}</span>
          </div>
        ))}
      </div>
      <button className={styles.navButton} onClick={handleNext}>{'>'}</button>
    </div>
  );
};

export default NavegacaoDatas;
