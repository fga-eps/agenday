import React from 'react';
import styles from './TabelaAgendamentos.module.css';

const TabelaAgendamentos = ({ appointments }) => {
  return (
    <div className={styles.tabelaContainer}>
      <div className={styles.horarios}>
        {appointments.map((appointment, index) => (
          <div key={index} className={styles.row}>
            <div className={styles.horario}>
              <span className={styles.horarioLabel}>{appointment.time}</span>
            </div>
            <div className={styles.agendamentos}>
              <div className={styles.agendamento}>
                <div className={styles.agendamentoNome}>{appointment.clientName}</div>
                <div className={styles.agendamentoEspecialidade}>{appointment.employeeName}</div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default TabelaAgendamentos;
