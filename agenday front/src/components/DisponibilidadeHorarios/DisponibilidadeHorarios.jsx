import React, { useState, useEffect } from 'react';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import styles from './DisponibilidadeHorarios.module.css';

const path = import.meta.env.VITE_API_URL;


const DisponibilidadeHorarios = ({ profissionalId, onTimeSlotSelect }) => {
  const [selectedDate, setSelectedDate] = useState(null);
  const [availableSlots, setAvailableSlots] = useState([]);
  const [selectedSlot, setSelectedSlot] = useState(null);

  useEffect(() => {
    if (selectedDate) {
      const dateStr = selectedDate.toISOString().split('T')[0];
      axios.get(`${path}/registry/timeslots/employee/${profissionalId}?date=${dateStr}`)
        .then(response => {
          // Filtra apenas os horários disponíveis
          const availableSlots = response.data.filter(slot => slot.available);
          setAvailableSlots(availableSlots);
        })
        .catch(error => {
          console.error('Error fetching available slots', error);
        });
    }
  }, [selectedDate, profissionalId]);

  const handleDateChange = (date) => {
    setSelectedDate(date);
    setSelectedSlot(null); // Reset selected slot when date changes
  };

  const handleSlotSelect = (slot) => {
    setSelectedSlot(slot);
    if (onTimeSlotSelect) {
      onTimeSlotSelect(slot);
    }
  };

  return (
    <div className={styles.container}>
      <h2>Escolha o horário de atendimento </h2>
      <div className={styles.datePicker}>
        <label>Escolha uma data: </label>
        <DatePicker
          selected={selectedDate}
          onChange={handleDateChange}
          dateFormat="yyyy-MM-dd"
          className={styles.dateInput}
        />
      </div>
      <div className={styles.slotsContainer}>
        {availableSlots.map((slot, index) => (
          <button
            key={index}
            className={`${styles.slot} ${selectedSlot === slot ? styles.selected : ''}`}
            onClick={() => handleSlotSelect(slot)}
          >
            {slot.startTime} - {slot.endTime}
          </button>
        ))}
      </div>
    </div>
  );
};

export default DisponibilidadeHorarios;
