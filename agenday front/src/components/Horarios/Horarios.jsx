import React, { useState } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import styles from './Horarios.module.css';

const horarios = ['08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00'];

const Horarios = ({ comando, textoBotao, onTimeSlotChange }) => {
    const [selectedDate, setSelectedDate] = useState(null);
    const [selectedSlots, setSelectedSlots] = useState({});
    const [allTimeSlots, setAllTimeSlots] = useState([]);

    const handleDateChange = (date) => {
        setSelectedDate(date);
    };

    const handleSelect = (horario) => {
        setSelectedSlots(prevState => {
            const dateKey = selectedDate ? selectedDate.toISOString().split('T')[0] : '';
            const dateSlots = prevState[dateKey] || [];
            if (dateSlots.includes(horario)) {
                return {
                    ...prevState,
                    [dateKey]: dateSlots.filter(slot => slot !== horario)
                };
            } else {
                return {
                    ...prevState,
                    [dateKey]: [...dateSlots, horario]
                };
            }
        });
    };

    const handleAddTimeSlots = () => {
        const dateKey = selectedDate ? selectedDate.toISOString().split('T')[0] : '';
        if (selectedDate && selectedSlots[dateKey] && selectedSlots[dateKey].length > 0) {
            const slots = selectedSlots[dateKey].map(horario => ({
                data: dateKey,
                startTime: horario,
                endTime: `${(parseInt(horario.split(':')[0], 10) + 1).toString().padStart(2, '0')}:00`,
                available: true
            }));
            setAllTimeSlots([...allTimeSlots, ...slots]);
            setSelectedSlots({});
            setSelectedDate(null);
            if (onTimeSlotChange) {
                onTimeSlotChange([...allTimeSlots, ...slots]);
            }
        }
    };

    const handleCancel = () => {
        window.location.href = '/';
      };

    return (
        <div className={styles.tabelaContainer}>
            <h2 className={styles.title}>{comando}</h2>
            <div className={styles.datePicker}>
                <label>Escolha uma data: </label>
                <DatePicker
                    selected={selectedDate}
                    onChange={handleDateChange}
                    dateFormat="dd/MM/yyyy"
                    className={styles.dateInput}
                />
            </div>
            <div className={styles.horarios}>
                {horarios.map((horario, index) => (
                    <button 
                        key={index} 
                        className={`${styles.horario} ${selectedSlots[selectedDate && selectedDate.toISOString().split('T')[0]] && selectedSlots[selectedDate.toISOString().split('T')[0]].includes(horario) ? styles.selected : ''}`}
                        onClick={() => handleSelect(horario)}
                        disabled={!selectedDate}
                    >
                        {horario}
                    </button>
                ))}
            </div>
            <div className={styles.actions}>
                <button className={styles.addButton} onClick={handleAddTimeSlots} disabled={!selectedDate}>Adicionar Horários</button>
            </div>
            <div className={styles.selectedSlots}>
                <h3>Horários Selecionados:</h3>
                {allTimeSlots.map((slot, index) => (
                    <div key={index} className={styles.slotItem}>
                        {slot.data} - {slot.startTime} às {slot.endTime}
                    </div>
                ))}
            </div>
        </div>
    );
};

export default Horarios;