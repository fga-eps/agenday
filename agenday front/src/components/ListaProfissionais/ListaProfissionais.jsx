import React, { useState } from 'react';
import styles from './ListaProfissionais.module.css';

const ListaProfissionais = ({ professionals, selectedProfessional, onSelectProfessional }) => {
  const handleSelect = (name) => {
    onSelectProfessional(name);
  };

  return (
    <div className={styles.listaContainer}>
      {professionals.map((profissional, index) => (
        <div
          key={index}
          className={`${styles.profissional} ${selectedProfessional === profissional.name ? styles.selected : ''}`}
          onClick={() => handleSelect(profissional.name)}
        >
          <div className={`${styles.icon} ${selectedProfessional === profissional.name ? styles.selectedIcon : ''}`}></div>
          <div className={styles.info}>
            <span className={styles.nome}>{profissional.name}</span>
            <span className={styles.especialidade}>{profissional.specialty}</span>
          </div>
        </div>
      ))}
    </div>
  );
};

export default ListaProfissionais;
