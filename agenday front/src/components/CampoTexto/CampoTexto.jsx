import React from 'react';
import styles from './CampoTexto.module.css';

const CampoTexto = ({ label, type = "text", name, required = false }) => {
  return (
    <div className={styles.inputField}>
      <label>{label}{required && <span className={styles.required}>*</span>}</label>
      <input type={type} name={name} required={required} />
    </div>
  );
};

export default CampoTexto;
