import React from 'react';
import { Link } from 'react-router-dom';
import style from './Header.module.css';
import logo from '../../imagens/logo.png';
import { Button, TIPO_BUTTON } from '../../components';

const Header = () => {
  return (
    <header className={style.header}>
      <div className={style.logoContainer}>
        <img src={logo} alt="Logo" className={style.logo} />
        <Link to="/" className={style.titleLink}>
          <h2 className={style.title}>Agenday</h2>
        </Link>
      </div>
      <nav className={style.nav}>
        <Link to="/login" className={style.link}>
          <Button type={TIPO_BUTTON.SECONDARY} className={style.btnSecondary}>Entrar</Button>
        </Link>
        <Link to="/cadastro-usuario" className={style.link}>
          <Button type={TIPO_BUTTON.PRIMARY} className={style.btnPrimary}>Cadastrar</Button>
        </Link>
      </nav>
    </header>
  );
};

export default Header;
