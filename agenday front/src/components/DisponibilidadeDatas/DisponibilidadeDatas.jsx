import React from 'react';
import styles from './DisponibilidadeDatas.module.css';

const DisponibilidadeDatas = ({ timeSlots, onDateSelect }) => {
    const uniqueDates = [...new Set(timeSlots.map(slot => slot.date))];

    return (
        <div className={styles.DisponibilidadeDatas}>
            <h3>Selecione os dias de atendimento</h3>
            <div className={styles.datesContainer}>
                {uniqueDates.map(date => (
                    <button key={date} onClick={() => onDateSelect(date)}>{date}</button>
                ))}
            </div>
        </div>
    );
}

export default DisponibilidadeDatas;
