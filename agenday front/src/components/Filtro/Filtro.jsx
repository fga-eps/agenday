export {Filtro};

import React from 'react';
import { CampoTexto } from '../../components';
import style from './Filtro.module.css';

const Filtro = () => {
    return (
        <form className={style.Filtro}>
            <CampoTexto />
        </form>
    );
};

export default Filtro;
