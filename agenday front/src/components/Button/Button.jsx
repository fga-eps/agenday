import React from 'react';
import styles from './Button.module.css';

const Button = ({ type, className, children, ...props }) => {
  return (
    <button type={type} className={`${styles.Button} ${className}`} {...props}>
      {children}
    </button>
  );
};

export default Button;
