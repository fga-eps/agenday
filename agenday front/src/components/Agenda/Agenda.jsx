import React, { useState } from 'react';
import styles from './Agenda.module.css';

const Agenda = ({ title, tipo }) => {
  const [view, setView] = useState('Dia');

  return (
    <div className={styles.container}>
      <h1 className={styles.title}>{title}</h1>
      <div className={styles.filters}>
        <select className={styles.select}>
          <option>Selecione a especialidade</option>
          {/* Adicione as opções de especialidades aqui */}
        </select>
        <select className={styles.select}>
          <option>Selecione o profissional</option>
          {/* Adicione as opções de profissionais aqui */}
        </select>
      </div>
      {tipo === 'agenda' && (
        <div className={styles.viewToggle}>
          <button
            className={`${styles.button} ${view === 'Dia' ? styles.active : ''}`}
            onClick={() => setView('Dia')}
          >
            Dia
          </button>
          <button
            className={`${styles.button} ${view === 'Semana' ? styles.active : ''}`}
            onClick={() => setView('Semana')}
          >
            Semana
          </button>
        </div>
      )}
    </div>
  );
};

export default Agenda;
