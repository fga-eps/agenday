import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import style from './HeaderLoged.module.css';
import logo from '../../imagens/logo.png';
import { Button, TIPO_BUTTON } from '..';

const HeaderLoged = ({ onLogout }) => {
  const navigate = useNavigate();

  const handleLogoutClick = (e) => {
    e.preventDefault();
    onLogout();
    navigate('/');
  };

  return (
    <header className={style.headerLoged}>
      <div className={style.logoContainer}>
        <img src={logo} alt="Logo" className={style.logo} />
        <Link to="/home-instituicao" className={style.titleLink}>
          <h2 className={style.title}>Agenday</h2>
        </Link>
      </div>
      <nav className={style.nav}>
        <Button 
          type={TIPO_BUTTON.SECONDARY} 
          className={style.btnSecondary} 
          onClick={handleLogoutClick}
        >
          Sair
        </Button>
      </nav>
    </header>
  );
};

export default HeaderLoged;