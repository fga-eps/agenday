import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { Header, HeaderLoged } from './components';
import { HomeUsr, CadastroInstituicao, GerenciamentoEspecialidades, HomeInst, Login, Agendamento, DadosAgendamento, CadastroUsuario, VisualizarAgendamento, CadastroProfissional, HomeSeparacao, GerenciamentoInst } from './pages';

const App = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    const token = localStorage.getItem('Token');
    const id = localStorage.getItem('id');
    if (token && id) {
      setIsLoggedIn(true);
    }
  }, []);

  const handleLogout = () => {
    localStorage.removeItem('id');
    localStorage.removeItem('Token');
    setIsLoggedIn(false);
  };

  return (
    <Router>
      {isLoggedIn ? <HeaderLoged onLogout={handleLogout} /> : <Header />}
      <Routes>
        <Route path="/" element={<HomeSeparacao />} />
        <Route path='/home-instituicao' element={<HomeInst/>}/>
        <Route path="/cadastro-instituicao" element={<CadastroInstituicao />} />
        <Route path="/gerenciamento-inst" element={<GerenciamentoInst />} />
        <Route path="/gerenciamento-especialidades" element={<GerenciamentoEspecialidades />} />
        <Route path="/login" element={<Login />} />
        <Route path="/agendamento" element={<Agendamento/>} />
        <Route path="/dados-agendamento" element={<DadosAgendamento />} />
        <Route path="/cadastro-usuario" element={<CadastroUsuario />} />
        <Route path="/cadastro-profissional" element={<CadastroProfissional />} />
        <Route path="/visualizar-agendamento" element={<VisualizarAgendamento />} />
        <Route path="/home-usuario" element={<HomeUsr />} />
      </Routes>
    </Router>
  );
};

export { App };
