import axios from 'axios';

export default class UserService {
  constructor() {
    this.axios = axios.create({
      baseURL: import.meta.env.VITE_API_URL 
    });
  }

  async login(dados) {
    console.log('Dados recebidos no método login:', dados);
    try {
      const { data } = await this.axios.post('/user-management/login', dados);

      if (data && data.Token && data.id) {
        localStorage.setItem('Token', data.Token);
        localStorage.setItem('id', data.id);
        localStorage.setItem('instituitionId', data.instituitionId);

        console.log('Dados armazenados no localStorage:', {
          Token: data.Token,
          id: data.id
        });

        return true;
      }
      return false;
    } catch (error) {
      console.error('Login error:', error);
      return false;
    }
  }

  async cadastrarUsuario(dados) {
    // console.log('Dados recebidos no método login:', dados);
    try {
      const { data } = await this.axios.post('/user-management/create', dados);

      if (data && data.userId) {
        localStorage.setItem('id', data.userId);

        console.log('Dados armazenados no localStorage:', {
            userId: data.userId
        });

        return true;
      }
      return false;
    } catch (error) {
      console.error('Login error:', error);
      return false;
    }
  }

  async cadastrarInstituicao(dados) {
    try {
      const userId = localStorage.getItem('id');
  
      const { data } = await this.axios.post(`/registry/institution/create/by/user/${userId}`, dados);
  
      // Verifica se a mensagem de sucesso está presente na resposta
      if (data) {
        console.log('Institution created successfully');
        return true;
      }
      return false;
    } catch (error) {
      console.error('Error creating institution:', error);
      return false;
    }
  }  
}