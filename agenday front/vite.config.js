import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  server: {
    host: true,  // Permite que o servidor esteja acessível via IP ou localhost
    port: 3000,  // Define a porta 3000 para o servidor
  },
})
