package com.agenday.usermanagement.controller;

import com.agenday.usermanagement.dto.*;
import com.agenday.usermanagement.model.User;
import com.agenday.usermanagement.service.JwtTokenService;
import com.agenday.usermanagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user-management")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private JwtTokenService jwtTokenService;

    @PostMapping("/login")
    public ResponseEntity<RecoveryUserDto> authenticateUser(@RequestBody LoginUserDto loginUserDto) {
        RecoveryUserDto recoveryUserDto = userService.authenticateUser(loginUserDto);
        return new ResponseEntity<>(recoveryUserDto, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<UserResponseDTO> createUser(@RequestBody CreateUserDto createUserDto) throws Exception{
        UserResponseDTO userResponseDTO= userService.createUser(createUserDto);
        return new ResponseEntity<>(userResponseDTO,HttpStatus.CREATED);
    }

    @PostMapping("/validate")
    public ResponseEntity<String> ValidateUser(@RequestBody TokenDTO tokenDTO) throws Exception{
        String token = tokenDTO.getToken();
        String user= jwtTokenService.getSubjectFromToken(token);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

}