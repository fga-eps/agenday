package com.agenday.registry.dto.Services.Provided;

import com.agenday.registry.dto.GeolocationDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InstitutionProvided {
    private String name;
    GeolocationDTO geolocationDTO;
    private List<SpecialtiesProvidedDTO> speciaties;

}
