package com.agenday.registry.dto;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AppointmentResponseDTO {
    private String clientName;
    private String employeeName;
    private String date;
    private String time;
    private String Specialty;
}
