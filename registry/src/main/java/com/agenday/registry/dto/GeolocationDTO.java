package com.agenday.registry.dto;

public class GeolocationDTO {
    private double latitude;
    private double longitude;
    private String formattedAddress;

    public GeolocationDTO(double latitude, double longitude, String formattedAddress) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.formattedAddress = formattedAddress;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    @Override
    public String toString() {
        return "GeolocationDTO{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", formattedAddress='" + formattedAddress + '\'' +
                '}';
    }
}