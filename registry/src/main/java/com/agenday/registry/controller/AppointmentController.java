package com.agenday.registry.controller;

import com.agenday.registry.dto.AppointmentResponseDTO;
import com.agenday.registry.model.Appointment;
import com.agenday.registry.dto.AppointmentDTO;
import com.agenday.registry.service.AppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("registry/appointments")
public class AppointmentController {

    @Autowired
    private AppointmentService appointmentService;

    @PostMapping
    public ResponseEntity<Appointment> createAppointment(@RequestBody AppointmentDTO appointmentDTO) throws Exception{
        Appointment savedAppointment = appointmentService.save(appointmentDTO);
        return new ResponseEntity<>(savedAppointment, HttpStatus.CREATED);
    }
    @GetMapping("/institution/{institutionId}")
    public ResponseEntity<List<AppointmentResponseDTO>> getAppointmentsByInstitution(@PathVariable Long institutionId) {
        List<AppointmentResponseDTO> appointments = appointmentService.findByInstitutionId(institutionId);
        return ResponseEntity.ok(appointments);
    }
}