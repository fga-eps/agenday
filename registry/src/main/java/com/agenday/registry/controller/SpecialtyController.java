package com.agenday.registry.controller;

import com.agenday.registry.dto.SpecialtyDTO;
import com.agenday.registry.model.Address;
import com.agenday.registry.service.SpecialtyPopulateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import com.agenday.registry.model.Specialty;
import com.agenday.registry.service.SpecialtyService;

import java.util.List;

@RestController
@RequestMapping("registry/specialties")
public class SpecialtyController {

    @Autowired
    private SpecialtyService specialtyService;

    @Autowired
    private SpecialtyPopulateService specialtyPopulateService;

    @GetMapping
    public List<Specialty> getAllSpecialties() {
        return specialtyService.getAllSpecialties();
    }

    @PostMapping("/populate")
    public ResponseEntity<String> populateSpecialties() {
        try {
            specialtyPopulateService.populateSpecialties();
            return ResponseEntity.ok("Especialidades de saúde populadas com sucesso.");
        } catch (Exception e) {
            return ResponseEntity.status(409).body(e.getMessage());
        }
    }

    @PostMapping("/institutions/{institutionId}/add/{specialtyId}")
    public ResponseEntity<String> addSpecialtyToInstitution(@PathVariable Long institutionId, @PathVariable Long specialtyId) {
        try {
            specialtyService.addSpecialtyToInstitution(institutionId, specialtyId);
            return ResponseEntity.ok("Especialidade adicionada à instituição com sucesso.");
        } catch (Exception e) {
            return ResponseEntity.status(409).body(e.getMessage());
        }
    }

    @PostMapping("/institutions/{institutionId}/remove/{specialtyId}")
    public ResponseEntity<String> removeSpecialtyFromInstitution(@PathVariable Long institutionId, @PathVariable Long specialtyId) {
        try {
            specialtyService.removeSpecialtyFromInstitution(institutionId, specialtyId);
            return ResponseEntity.ok("Especialidade removida da instituição com sucesso.");
        } catch (Exception e) {
            return ResponseEntity.status(409).body(e.getMessage());
        }
    }

    @GetMapping("/institutions/{institutionId}")
    public ResponseEntity<List<Specialty>> getSpecialtiesByInstitution(@PathVariable Long institutionId) {
        try {
            List<Specialty> specialties = specialtyService.getSpecialtiesByInstitution(institutionId);
            return ResponseEntity.ok(specialties);
        } catch (Exception e) {
            return ResponseEntity.status(404).body(null);
        }
    }
}
