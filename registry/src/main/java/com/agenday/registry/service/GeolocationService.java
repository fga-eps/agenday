package com.agenday.registry.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import com.agenday.registry.dto.GeolocationDTO;
import org.json.JSONObject;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class GeolocationService {
    private static final String API_KEY = "AIzaSyD5UunSEbfFmAceUjXh8gjwarvObtHQKng";  // Substitua com sua chave de API
    private static final Logger logger = LoggerFactory.getLogger(GeolocationService.class);

    public GeolocationDTO getGeolocation(String address) throws Exception {
        String encodedAddress = URLEncoder.encode(address, "UTF-8");
        String urlString = "https://maps.googleapis.com/maps/api/geocode/json?address=" + encodedAddress + "&key=" + API_KEY;
        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");

        int responseCode = conn.getResponseCode();
        if (responseCode != 200) {
            logger.error("Failed : HTTP error code : " + responseCode);
            return null;
        }

        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        JSONObject jsonResponse = new JSONObject(response.toString());
        JSONArray results = jsonResponse.getJSONArray("results");
        if (results.length() > 0) {
            JSONObject result = results.getJSONObject(0);
            JSONObject geometry = result.getJSONObject("geometry");
            JSONObject location = geometry.getJSONObject("location");

            double lat = location.getDouble("lat");
            double lng = location.getDouble("lng");
            String formattedAddress = result.getString("formatted_address");

            return new GeolocationDTO(lat, lng, formattedAddress);
        } else {
            logger.error("No results found for the given address.");
            return null;  // ou você pode retornar um objeto de erro apropriado
        }
    }

}
