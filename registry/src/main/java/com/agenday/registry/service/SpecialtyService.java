package com.agenday.registry.service;

import com.agenday.registry.model.Institution;
import com.agenday.registry.model.Specialty;
import com.agenday.registry.repository.InstitutionRepository;
import com.agenday.registry.repository.SpecialtyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class SpecialtyService {

    @Autowired
    private SpecialtyRepository specialtyRepository;

    @Autowired
    private InstitutionRepository institutionRepository;

    public List<Specialty> getAllSpecialties() {
        return specialtyRepository.findAll();
    }

    public Specialty saveSpecialty(Specialty specialty) {
        return specialtyRepository.save(specialty);
    }

    public Specialty getSpecialtyById(Long id) {
        return specialtyRepository.findById(id).orElse(null);
    }

    public void deleteSpecialty(Long id) {
        specialtyRepository.deleteById(id);
    }

    @Transactional
    public void addSpecialtyToInstitution(Long institutionId, Long specialtyId) throws Exception {
        Optional<Institution> institutionOptional = institutionRepository.findById(institutionId);
        Optional<Specialty> specialtyOptional = specialtyRepository.findById(specialtyId);

        if (institutionOptional.isPresent() && specialtyOptional.isPresent()) {
            Institution institution = institutionOptional.get();
            Specialty specialty = specialtyOptional.get();
            institution.getSpecialties().add(specialty);
            institutionRepository.save(institution);
        } else {
            throw new Exception("Instituição ou Especialidade não encontrada");
        }
    }

    @Transactional
    public void removeSpecialtyFromInstitution(Long institutionId, Long specialtyId) throws Exception {
        Optional<Institution> institutionOptional = institutionRepository.findById(institutionId);
        Optional<Specialty> specialtyOptional = specialtyRepository.findById(specialtyId);

        if (institutionOptional.isPresent() && specialtyOptional.isPresent()) {
            Institution institution = institutionOptional.get();
            Specialty specialty = specialtyOptional.get();
            institution.getSpecialties().remove(specialty);
            institutionRepository.save(institution);
        } else {
            throw new Exception("Instituição ou Especialidade não encontrada");
        }
    }
    public List<Specialty> getSpecialtiesByInstitution(Long institutionId) throws Exception {
        Optional<Institution> institutionOptional = institutionRepository.findById(institutionId);
        if (institutionOptional.isPresent()) {
            return institutionOptional.get().getSpecialties();
        } else {
            throw new Exception("Instituição não encontrada");
        }
    }
}
