package com.agenday.registry.service;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class SmsService {
    private static final Logger logger = LoggerFactory.getLogger(SmsService.class);

    private static final String ACCOUNT_SID = "ACffbceea1edc7c2704b50c817ff8c03de";
    private static final String AUTH_TOKEN = "f768313bed44ec3bd1f65707cced886d";  // Substitua com seu Auth Token
    private static final String FROM_PHONE_NUMBER = "+19706606073";  // Substitua com seu número de telefone Twilio

    public void sendSms(String to, String body) throws Exception {
        String urlString = "https://api.twilio.com/2010-04-01/Accounts/" + ACCOUNT_SID + "/Messages.json";
        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);

        String auth = ACCOUNT_SID + ":" + AUTH_TOKEN;
        String encodedAuth = Base64.getEncoder().encodeToString(auth.getBytes());
        conn.setRequestProperty("Authorization", "Basic " + encodedAuth);

        String urlParameters = "To=" + to + "&From=" + FROM_PHONE_NUMBER + "&Body=" + body;
        logger.info("URL Parameters: " + urlParameters);  // Log dos parâmetros

        byte[] postData = urlParameters.getBytes();
        OutputStream os = conn.getOutputStream();
        os.write(postData);
        os.flush();
        os.close();

        int responseCode = conn.getResponseCode();
        logger.info("Response Code: " + responseCode);

        if (responseCode != HttpURLConnection.HTTP_CREATED) {
            logger.error("Error Envio SMS: ");
        } else {
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            logger.info("Response: " + response.toString());
        }
    }
}
