package com.agenday.registry.service;

import com.agenday.registry.model.Specialty;
import com.agenday.registry.repository.SpecialtyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SpecialtyPopulateService {

    @Autowired
    private SpecialtyRepository specialtyRepository;

    @Transactional
    public void populateSpecialties() throws Exception {
        long count = specialtyRepository.count();
        if(count == 0) {
            List<Specialty> specialties = List.of(
                    new Specialty(null, "Alergologia", "Alergologia", null, null),
                    new Specialty(null, "Anestesiologia", "Anestesiologia", null, null),
                    new Specialty(null, "Angiologia", "Angiologia", null, null),
                    new Specialty(null, "Cardiologia", "Cardiologia", null, null),
                    new Specialty(null, "Cirurgia Cardiovascular", "Cirurgia Cardiovascular", null, null),
                    new Specialty(null, "Cirurgia de Cabeça e Pescoço", "Cirurgia de Cabeça e Pescoço", null, null),
                    new Specialty(null, "Cirurgia Geral", "Cirurgia Geral", null, null),
                    new Specialty(null, "Cirurgia Pediátrica", "Cirurgia Pediátrica", null, null),
                    new Specialty(null, "Cirurgia Plástica", "Cirurgia Plástica", null, null),
                    new Specialty(null, "Cirurgia Torácica", "Cirurgia Torácica", null, null),
                    new Specialty(null, "Cirurgia Vascular", "Cirurgia Vascular", null, null),
                    new Specialty(null, "Clínica Médica", "Clínica Médica", null, null),
                    new Specialty(null, "Dermatologia", "Dermatologia", null, null),
                    new Specialty(null, "Endocrinologia", "Endocrinologia", null, null),
                    new Specialty(null, "Gastroenterologia", "Gastroenterologia", null, null),
                    new Specialty(null, "Genética Médica", "Genética Médica", null, null),
                    new Specialty(null, "Geriatria", "Geriatria", null, null),
                    new Specialty(null, "Ginecologia", "Ginecologia", null, null),
                    new Specialty(null, "Hematologia", "Hematologia", null, null),
                    new Specialty(null, "Hepatologia", "Hepatologia", null, null),
                    new Specialty(null, "Infectologia", "Infectologia", null, null),
                    new Specialty(null, "Medicina de Família e Comunidade", "Medicina de Família e Comunidade", null, null),
                    new Specialty(null, "Medicina do Trabalho", "Medicina do Trabalho", null, null),
                    new Specialty(null, "Medicina Esportiva", "Medicina Esportiva", null, null),
                    new Specialty(null, "Medicina Física e Reabilitação", "Medicina Física e Reabilitação", null, null),
                    new Specialty(null, "Medicina Intensiva", "Medicina Intensiva", null, null),
                    new Specialty(null, "Medicina Nuclear", "Medicina Nuclear", null, null),
                    new Specialty(null, "Nefrologia", "Nefrologia", null, null),
                    new Specialty(null, "Neonatologia", "Neonatologia", null, null),
                    new Specialty(null, "Neurologia", "Neurologia", null, null),
                    new Specialty(null, "Nutrologia", "Nutrologia", null, null),
                    new Specialty(null, "Obstetrícia", "Obstetrícia", null, null),
                    new Specialty(null, "Oftalmologia", "Oftalmologia", null, null),
                    new Specialty(null, "Oncologia", "Oncologia", null, null),
                    new Specialty(null, "Ortopedia", "Ortopedia", null, null),
                    new Specialty(null, "Otorrinolaringologia", "Otorrinolaringologia", null, null),
                    new Specialty(null, "Pediatria", "Pediatria", null, null),
                    new Specialty(null, "Pneumologia", "Pneumologia", null, null),
                    new Specialty(null, "Psiquiatria", "Psiquiatria", null, null),
                    new Specialty(null, "Radiologia", "Radiologia", null, null),
                    new Specialty(null, "Reumatologia", "Reumatologia", null, null),
                    new Specialty(null, "Urologia", "Urologia", null, null)
            );
            specialtyRepository.saveAll(specialties);
        } else {
            throw new Exception("Banco de dados já contém especialidades cadastradas.");
        }
    }
}
