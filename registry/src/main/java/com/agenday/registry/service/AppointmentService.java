package com.agenday.registry.service;

import com.agenday.registry.dto.AppointmentResponseDTO;
import com.agenday.registry.exception.ForbiddenException;
import com.agenday.registry.model.*;
import com.agenday.registry.dto.AppointmentDTO;
import com.agenday.registry.repository.AppointmentRepository;
import com.agenday.registry.repository.ClientRepository;
import com.agenday.registry.repository.EmployeeRepository;
import com.agenday.registry.repository.TimeSlotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AppointmentService {

    @Autowired
    private AppointmentRepository appointmentRepository;
    @Autowired
    private TimeSlotRepository timeSlotRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private SmsService smsService;
    @Autowired
    private ClientRepository clientRepository;

    public Appointment save(AppointmentDTO appointmentDTO) throws Exception {
        Employee employee = employeeRepository.findById(appointmentDTO.getEmployeeId())
                .orElseThrow(() -> new ForbiddenException("Error saving institution:"));

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalDate date = LocalDate.parse(appointmentDTO.getDate(), dateFormatter);
        LocalTime time = LocalTime.parse(appointmentDTO.getTime(), timeFormatter);

        Client client = Client.builder()
                .cpf(appointmentDTO.getClient().getCpf())
                .email(appointmentDTO.getClient().getEmail())
                .name(appointmentDTO.getClient().getName())
                .dateBirth(appointmentDTO.getClient().getDateBirth())
                .phone(appointmentDTO.getClient().getPhone())
                .build();
        client = clientRepository.save(client);

        // Verificar se o horário está disponível na data especificada
        TimeSlot timeSlot = timeSlotRepository.findByEmployeeIdAndDateAndStartTime(
                employee.getId(),
                date,
                time
        );

        if (timeSlot == null || !timeSlot.isAvailable()) {
            throw new ForbiddenException("Horário não disponível");
        }

        // Marcar o intervalo de tempo como não disponível
        timeSlot.setAvailable(false);
        timeSlotRepository.save(timeSlot);

        Appointment appointment = new Appointment();
        appointment.setEmployee(employee);
        appointment.setDate(date);
        appointment.setTime(time);
        appointment.setClient(client);

        String to = appointmentDTO.getClient().getPhone();
        to = "+55" + to;
        String nameEmploye = employee.getName();
        String nameClient = client.getName();
        if (to != null && nameEmploye != null) {
            String body = String.format("Olá %s, seu agendamento com %s está confirmado para %s às %s.", nameClient, nameEmploye, date, time);
            smsService.sendSms(to, body);
        }
        return appointmentRepository.save(appointment);
    }

    public List<Appointment> findByEmployeeId(Long employeeId) {
        return appointmentRepository.findByEmployeeId(employeeId);
    }

    public List<AppointmentResponseDTO> findByInstitutionId(Long institutionId) {
        try {
            List<Appointment> appointments = appointmentRepository.findByInstitutionId(institutionId);
            return appointments.stream().map(appointment -> {
                AppointmentResponseDTO dto = new AppointmentResponseDTO();
                dto.setClientName(appointment.getClient().getName());
                dto.setEmployeeName(appointment.getEmployee().getName());
                String specialty = appointment.getEmployee().getSpecialties().stream().findFirst().get().getName();
                dto.setSpecialty(specialty);
                dto.setDate(appointment.getDate().toString());
                dto.setTime(appointment.getTime().toString());
                return dto;
            }).collect(Collectors.toList());
        } catch (Exception e) {
            throw new ForbiddenException("Error found appointments: " + e.getMessage());
        }
    }
}
