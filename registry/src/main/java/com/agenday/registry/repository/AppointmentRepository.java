package com.agenday.registry.repository;

import com.agenday.registry.model.Appointment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AppointmentRepository extends JpaRepository<Appointment, Long> {
    List<Appointment> findByEmployeeId(Long employeeId);
    @Query("SELECT a FROM Appointment a JOIN a.employee e WHERE e.institution.id = :institutionId")
    List<Appointment> findByInstitutionId(@Param("institutionId") Long institutionId);
}
