## Sobre

![Imagem Logo](docs/assets/agendalogo.png)

O Agenday é uma Aplicação Web cuja função é ajudar o cliente e as empresas facilitando o processo de agendamento de serviços.

**GitLab do projeto dispinível em:** <a href="https://gitlab.com/fga-eps/agenday"><u>Agenday - GitLab</u></a>

**Acesse o serviço em:** <a href="https://agenday-front-575078153a88.herokuapp.com/"><u>Agenday</u></a>
## Integrantes

| Membros                    | Função        |
| -------------------------- | ------------- |
| Carla Rocha Cangussú       | Desenvolvedor |
| Felipe Correia Andrade     | Desenvolvedor |
| João Paulo Coelho de Souza | Desenvolvedor |
| Mateus Brandão Teixeira    | Desenvolvedor |
